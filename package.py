name = "fileManager"

version = "1.0.0"

authors = ['ArtFx TD gang']

description = \
    """

    """

requires = [
    "python",
    # "node", for server build
    # "yarn", for install / build
]

vcs = "git"

def commands():
    from sys import platform
    global env
    plarform_str = "".join([i for i in platform if not i.isdigit()])
    env.PATH.append("{root}/filemanager/dist/" + plarform_str + "-unpacked")
