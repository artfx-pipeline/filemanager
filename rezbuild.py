import os
import shutil
import subprocess
from sys import platform

def build(source_path, build_path, install_path, targets):

    plarform_str = "".join([i for i in platform if not i.isdigit()])

    def _build():
        """
        Build the javascript package
        """
        src = os.path.join(source_path, "filemanager", "dist", plarform_str + "-unpacked")
        dest = os.path.join(build_path, "filemanager", "dist", plarform_str + "-unpacked")
        print("=" * 30)
        print("Yarn install")
        print("=" * 30)
        result = subprocess.call(["yarn", "install"], shell=True, cwd=os.path.join(source_path, "filemanager"))
        if result != 0:
            raise RuntimeError("Yarn install failed.")
        print("=" * 30)
        print("Yarn build")
        print("=" * 30)
        result = subprocess.call(["yarn", "build"], shell=True, cwd=os.path.join(source_path, "filemanager"))
        if result != 0:
            raise RuntimeError("Yarn build failed.")
        print("=" * 30)
        print("Copy dist into build path")
        print("=" * 30)
        print("{0} => {1}".format(src, dest))
        if os.path.exists(dest):
            shutil.rmtree(dest)
        shutil.copytree(src, dest)

    def _install():
        src = os.path.join(build_path, "filemanager", "dist", plarform_str + "-unpacked")
        dest = os.path.join(install_path, "filemanager", "dist", plarform_str + "-unpacked")
        print("=" * 30)
        print("Copy dist into install path")
        print("=" * 30)
        print("{0} => {1}".format(src, dest))
        if os.path.exists(dest):
            shutil.rmtree(dest)
        shutil.copytree(src, dest)

    _build()

    if "install" in (targets or []):
        _install()
