import React from 'react';
import Manager from './pages/manager'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Hello world</h1>
        <Manager />
      </header>
    </div>
  );
}

export default App;
